import React from 'react';
import { BrowserRouter } from "react-router-dom";
import 'antd/dist/antd.css';
import './index.css';
import MenuBar from '../menu_bar';
import ContentPage from '../content_page';
export default class AppWrapper extends React.Component {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };

    render() {
        return (
            <div className="App-Wrapper">
                <BrowserRouter>
                    <MenuBar />
                    <ContentPage />
                </BrowserRouter>
            </div>
        );
    }
}
