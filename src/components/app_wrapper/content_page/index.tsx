import React from 'react';
import { Switch, Route} from "react-router-dom";
import Home from '../../home/page';
import Tools from '../../tools/page';
import About from '../../about/page';

export default class MenuBar extends React.Component {
    render() {
        return (
            <Switch>
                <Route path="/home" component={Home} />
                <Route path="/tools" component={Tools} />
                <Route path="/about" component={About} />
            </Switch>
        )
    }

}
