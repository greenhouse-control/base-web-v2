import React, { Component } from 'react';
import {DataProfile} from '../../../access';
import 'antd/dist/antd.css';

type TypeOfData = {
    time: string,
    temperature: string,
    humidity: string,
    light: string,
    isLoading: boolean,
}

export default class SensorsNow extends Component<{},TypeOfData> {
    interval_id: any
    
    constructor(props: any) {
        super(props);
        this.state = {
            time: '',
            temperature: '',
            humidity: '',
            light: '',
            isLoading: false,
        };
    }

    componentDidMount(){
        
        const API_SENSORS_NOW = 'sensors-now';
        const getData = async() =>  {
            try {
                this.setState({isLoading: true});
                const resp =  await DataProfile.Get(API_SENSORS_NOW);
                this.state.isLoading &&  this.setState({
                    time: resp.data['0']['time'],
                    temperature: resp.data['0']['temperature'],
                    humidity: resp.data['0']['humidity'],
                    light: resp.data['0']['light'],
                });
            } catch(error) {
                console.log('Error', error);
            } finally {
                this.setState({isLoading: false});
            }
           
        }
        getData();
        this.interval_id = setInterval(() => {getData()}, 5000);
    }        

    componentWillUnmount(){
        clearInterval(this.interval_id)
    }
   
    render(){
        return(
            <div>
               <h2>Sensors Now</h2>
                <ul>
                    <li><span>Time: </span>{this.state.time} </li>
                    <li><span>Temperature: </span>{this.state.temperature}<span> °C</span></li>
                    <li><span>Humidity: </span>{this.state.humidity}<span> %RH</span></li>
                    <li><span>Light: </span>{this.state.light}<span> lx</span></li>
                </ul>
            </div>    
        )
    }

}