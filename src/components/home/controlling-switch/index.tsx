import React, { Component } from 'react';
import { Switch, Button, Popconfirm, message } from 'antd';
import {DataProfile} from '../../../access';

type TypeOfSwitch = {
    relay1: boolean,
    relay2: boolean,
    isLock1: boolean,
    isLock2: boolean,
}
export default class ControllingSwitch extends Component <{},TypeOfSwitch> {
    constructor(props: any) {
        super(props);
        this.state = {
            relay1 : false,
            relay2 : false,
            isLock1: true,  
            isLock2: true, 
        };
    } 
    
    myMessenger = (name: string, status: boolean) => {
        status? message.success({ content: `${name} is ON!`, duration: 2 }):
                message.error({content: `${name} is OFF!`, duration: 2})
    }
    toggleConfirm1 = () => {
        this.setState({
          isLock1: false
        });
      };
    toggleConfirm2 = () => {
        this.setState({
            isLock2: false
        });
    };
    toggleCancel1 = () => {
        this.setState({
          isLock1: true
        });
      };
    toggleCancel2 = () => {
        this.setState({
          isLock2: true
        });
      };
    
    onChange1 = (relay: boolean) => {
        relay ? DataProfile.Post('switch/relay1/on'): DataProfile.Post('switch/relay1/off')
        this.myMessenger('relay1',relay)  
        this.setState({ isLock1: true })       
        console.log(`switch 1 to ${relay}`)
      }
    onChange2 = (relay: boolean) => {
        relay ? DataProfile.Post('switch/relay2/on'): DataProfile.Post('switch/relay2/off')
        this.myMessenger('relay2',relay)  
        this.setState({ isLock2: true })  
        console.log(`switch 2 to ${relay}`);
     }
    myPopConfirm = (onConfirm: any, onCancel: any, isLock: boolean) => {
        return(
            <Popconfirm 
                placement="topLeft" 
                title={'Change status switch:'} 
                onConfirm={onConfirm} 
                onCancel={onCancel}
                okText="Unlock Switch" 
                cancelText="Lock Switch">
                <Button type="primary">
                    {isLock ? 'Locked Switch' : 'Unlocked Switch'}
                </Button>    
            </Popconfirm>
        )
    }

    render() {
        return (
          <div>
                {this.myPopConfirm(this.toggleConfirm1, this.toggleCancel1, this.state.isLock1)}
                <span > -- Relay 1 -- </span>
                <Switch 
                    disabled={this.state.isLock1} 
                    onChange={this.onChange1} 
                    defaultChecked={false}
                />
                <br/><br/>

                {this.myPopConfirm(this.toggleConfirm2, this.toggleCancel2, this.state.isLock2)}
                <span> -- Relay 2 -- </span>
                <Switch 
                    disabled={this.state.isLock2} 
                    onChange={this.onChange2} 
                    defaultChecked={false}
                />

          </div>
        );
      }
}