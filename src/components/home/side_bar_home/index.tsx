import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { Link } from "react-router-dom";
const { Sider } = Layout;


export default class SideBarHome extends Component<{ match: any },{}> {
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };
    render() {
        return (
            
            <Sider trigger={null} collapsible collapsed={this.state.collapsed} >
                <Menu  mode="inline" defaultSelectedKeys={['1']} >
                    <Menu.Item style={{ color: '#ffffb8' }}>
                        <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                        />
                    </Menu.Item>

                    <Menu.Item key="1">
                        <Link to={`${this.props.match.url}/monitoring`} style={{ color: '#bae7ff' }}>
                            <Icon type="desktop" />
                            <span>Monitoring</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="2">
                        <Link to={`${this.props.match.url}/controlling-switch`} style={{ color: '#d9f7be' }}>
                            <Icon type="control" />
                            <span>Controlling Switch</span>
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="3">
                        <Link to={`${this.props.match.url}/setting`} style={{ color: '#efdbff' }}>
                            <Icon type="setting" />
                            <span>Setting</span>
                        </Link>
                    </Menu.Item>
                </Menu>
            </Sider>
            
        )
    }

}
