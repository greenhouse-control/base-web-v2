import React from 'react';
import { Layout } from 'antd';
import { Route } from "react-router-dom";
import { Topic } from '../../tools/topic';
import SensorsNow from '../sensors_now';
import ControllingSwitch from '../controlling-switch';

const { Content } = Layout;
export default class ContentHome extends React.Component<{match: any},{}> {
    render() {
        return (
            
            <Content>
                <div style={{padding: '24px 24px 48px 24px'}}>
                    <Route
                        path={`${this.props.match.url}/:topicId`}
                        component={Topic} />
                    
                    <Route
                        path={`${this.props.match.url}/monitoring`}
                        component={SensorsNow} />
                    <Route
                        path={`${this.props.match.url}/controlling-switch`}
                        component={ControllingSwitch} />
                        
                    <Route
                        exact
                        path={this.props.match.url}
                        render={() =>
                            <h3>
                                Please select a topic.
                            </h3>
                        }
                    />
                </div>
            </Content>
            
        )
    }

}
