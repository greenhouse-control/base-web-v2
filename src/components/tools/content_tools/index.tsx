import React from 'react';
import { Layout } from 'antd';
import { Route } from "react-router-dom";
import { Topic } from '../../tools/topic';

const { Content } = Layout;
export default class ContentHome extends React.Component<{match: any},{}> {
    render() {
        return (
            
            <Content>
                <div style={{padding: '24px 24px 48px 24px'}}>
                    <Route
                        path={`${this.props.match.url}/:topicId`}
                        component={Topic} />
                    <Route
                        exact
                        path={this.props.match.url}
                        render={() =>
                            <h3>
                                Please select a topic.
                                <br/>
                                <br/>br Lorem ipsum, dolor sit amet consectetur adipisicing elit. Inventore dolores, recusandae quod similique veritatis excepturi explicabo eaque ex fugiat porro at impedit sit asperiores eveniet iusto voluptas itaque eum velit!
                                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
                                <br/>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Inventore dolores, recusandae quod similique veritatis excepturi explicabo eaque ex fugiat porro at impedit sit asperiores eveniet iusto voluptas itaque eum velit!
                                <br/>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Inventore dolores, recusandae quod similique veritatis excepturi explicabo eaque ex fugiat porro at impedit sit asperiores eveniet iusto voluptas itaque eum velit!
                            </h3>
                        }
                    />
                </div>
            </Content>
            
        )
    }

}
