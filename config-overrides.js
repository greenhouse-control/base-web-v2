const { override, fixBabelImports, addLessLoader } = require('customize-cra');
 
module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { 
        'primary-color': 'rgb(0,0,255)',
        'text-color': '#fff',
        'text-color-secondary': '#fff',
        'heading-color': '#fff',
        'font-size-base': '18px',
        'item-active-bg': 'rgba(184, 209, 252,0.2)',
        'input-number-handler-hover-bg': 'red',
        'component-background': 'rgba(0,0,0,0)',
        'menu-bg': 'rgba(0,0,0,0)',
        'layout-body-background': 'rgba(0,0,0,0)',
        'layout-header-background': 'rgba(0,0,0,0)',
        'popover-bg': 'rgba(155, 187, 242, 0.9)',
        'btn-primary-bg': '#389e0d',
        'switch-color': 'rgba(255, 0, 0, 0.7)',
        'card-background': 'rgba(0,0,0,0)',
        'card-actions-background': 'rgba(0,0,0,0)',

    },
  }),
);